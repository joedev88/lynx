var MONTHS = ["jan", "fév", "mar", "avr", "mai", "juin", "juil", "aoû", "sep", "oct", "nov", "déc"];
$(function() {
    startMonth = 1;
    startYear = 2020;
    endMonth = 0;
    endYear = 2021;
    nbreCal = 3;

    if (startMonth < 10)
        startDate = parseInt("" + startYear + '0' + startMonth + "");
    else
        startDate = parseInt("" + startYear + startMonth + "");
    if (endMonth < 10)
        endDate = parseInt("" + endYear + '0' + endMonth + "");
    else
        endDate = parseInt("" + endYear + endMonth + "");

    var d = new Date();
    content = '<div class="row lynx-calendarholder">';
    content += '<i class="lynx-yeardown arrow fas fa-chevron-left"></i>';

    for (y = 0; y < nbreCal; y++) {
        content += '<div class="col-xs-6" ><div class="lynx-calendar row" id="lynx-calendar-' + (y + 1) + '">' +
            '<h5 class="col-xs-12">' +
            '  <span class="year"> ' + (startYear + y) +
            '</h5><div class="lynx-monthsContainer"><div class="lynx-MonthsWrapper">';
        for (m = 0; m < 12; m++) {
            var monthval;
            if ((m + 1) < 10)
                monthval = "0" + (m + 1);
            else
                monthval = "" + (m + 1);
            content += '<span data-month="' + monthval + '" class="col-xs-3 lynx-month">' + MONTHS[m] + '</span>';
        }
        content += '</div></div></div></div>';
    }
    content += '<i class="lynx-yearup arrow fas fa-chevron-right"></i>';



    $(document).on('click', '.lynx-yeardown', function(e) {
        e.stopPropagation();
        $(this).nextAll(':has(.year)').find('.year').each(function() {
            var year = parseInt($(this).html());
            year--;
            $(this).html("" + year);
        });


        //effectMonths();
        $(this).children('.lynx-calendar').find('.lynx-MonthsWrapper').fadeOut(175, function() {
            effectMonths();
            $(this).children('.lynx-calendar').find('.lynx-MonthsWrapper').fadeIn(175);
        });
    });


    $(document).on('click', '.lynx-yearup', function(e) {
        e.stopPropagation();
        $(this).prevAll(':has(.year)').find('.year').each(function() {
            var year = parseInt($(this).html());
            year++
            $(this).html("" + year);
        });

        $(this).children('.lynx-calendar').find('.lynx-MonthsWrapper').fadeOut(175, function() {
            //  effectMonths();
            $(this).children('.lynx-calendar').find('.lynx-MonthsWrapper').fadeIn(175);
        });
    });

    $(document).on('click', '.lynx-month', function(e) {
        e.stopPropagation();

        $month = $(this);
        var monthnum = $month.data('month');
        var year = $month.parents('.lynx-calendar').children('h5').children('span').html();
        if ($month.parents('#lynx-calendar-1').size() > 0) {
            //Calcul start date
            startDate = parseInt("" + year + monthnum);
            if (startDate > endDate) {
                if (year != parseInt(endDate / 100))
                    $('.lynx-calendar:last h5 span').html(year);
                endDate = startDate;
            }
        } else {
            //Calcul end date
            endDate = parseInt("" + year + monthnum);
            if (startDate > endDate) {
                if (year != parseInt(startDate / 100))
                    $('.lynx-calendar:first h5 span').html(year);
                startDate = endDate;
            }
        }

        effectMonths();
    });

    var lynxVisible = false;
    $('.lynx-container').popover({
        container: "body",
        placement: "bottom",
        html: true,
        content: content
    }).on('show.bs.popover', function() {
        $('.popover').remove();
        var waiter = setInterval(function() {
            if ($('.popover').size() > 0) {
                clearInterval(waiter);
                effectMonths();
            }
        }, 50);

    }).on('shown.bs.popover', function() {
        lynxVisible = true;
    }).on('hidden.bs.popover', function() {
        lynxVisible = false;
    });

    $(document).on('click', '.lynx-calendarholder', function(e) {
        e.preventDefault();
        e.stopPropagation();
        lynxVisible = false;
    });
    $(document).on("click", ".lynx-container", function(e) {
        clear();
        if (lynxVisible) {
            e.preventDefault();
            e.stopPropagation();
            lynxVisible = false;
        }
    });
    $(document).on("click", function(e) {
        if (lynxVisible) {
            $('.lynx-calendarholder').parents('.popover').fadeOut(200, function() {
                $('.lynx-calendarholder').parents('.popover').remove();
                $('.lynx-container').trigger('click');
            });
            lynxVisible = false;
        }
    });
});


function clear() {
    $('.lynx-month').each(function(i) {
        $(this).removeClass('lynx-selected');
    });
}

function effectMonths() {
    $('#lynx-valid').removeClass('lynx-valid-active');
    $('.lynx-calendar').each(function() {
        var $cal = $(this);
        var year = $('h5 span', $cal).html();

        $('.lynx-month', $cal).each(function(i) {
            $(this).removeClass('lynx-selected');
            $('#lynx-valid').removeClass('lynx-valid-active');
            if ((i + 1) > 9)
                cDate = parseInt("" + year + (i + 1));
                ///to do calcul middle cal
                // if ((i + 1) > 18)
            // cDate = parseInt("" + year + (i + 1));
            else
                cDate = parseInt("" + year + '0' + (i + 1));
            if (cDate >= startDate && cDate <= endDate) {
                $(this).addClass('lynx-selected');
                $(this).removeClass('range-date');


            } else {
                $(this).removeClass('lynx-selected');
                $(this).removeClass('range-date');

            }
        });
    });
    var startyear = parseInt(startDate / 100);
    var startmonth = parseInt(safeRound((startDate / 100 - startyear)) * 100);
    var endyear = parseInt(endDate / 100);
    var endmonth = parseInt(safeRound((endDate / 100 - endyear)) * 100);
    $('.lynx-monthdisplay .lynx-lowerMonth').html(MONTHS[startmonth - 1] + " " + startyear);
    $('.lynx-monthdisplay .lynx-upperMonth').html(MONTHS[endmonth - 1] + " " + endyear);


    if (MONTHS[endmonth - 1])
        $('#lynx-valid').addClass('lynx-valid-active');


    $('.lynx-lowerDate').val(startDate);
    $('.lynx-upperDate').val(endDate);
    if (startyear == parseInt($('.lynx-calendar:first h5 span').html()))
        $('.lynx-calendar:first .lynx-selected:first').addClass("range-date");
    if (endyear == parseInt($('.lynx-calendar:last h5 span').html()))
        $('.lynx-calendar:last .lynx-selected:last').addClass("range-date");
}

function safeRound(val) {
    return Math.round(((val) + 0.00001) * 100) / 100;
}
